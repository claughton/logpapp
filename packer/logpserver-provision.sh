# An example basic image to use with logpapp
#
# NB the installing user is assumed to be "logp"
#
sleep 30
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y python3-pip nginx python3-dev build-essential libssl-dev libffi-dev python3-setuptools python3-venv openbabel
# Make a virtual environment
cd 
python3 -m venv logpenv
source logpenv/bin/activate
# Install REX and Gunicorn in the virtual environment
pip install wheel 
pip install gunicorn
git clone https://bitbucket.org/claughton/logpapp.git
cd logpapp
pip install -r requirements.txt
# Create the systemd service for gunicorn
cd
cat > logp.service << %EOF%
[Unit]
Description=Gunicorn instance to serve logpapp
After=network.target

[Service]
User=logp
Group=www-data
WorkingDirectory=/home/logp/logpapp
Environment="PATH=/home/logp/logpenvenv/bin:$PATH"
ExecStart=/home/logp/logpenv/bin/gunicorn --workers 3 --bind unix:logp.sock -m 007 logpapp:app

[Install]
WantedBy=multi-user.target
%EOF%
sudo cp logp.service /etc/systemd/system
sudo systemctl start logp
sudo systemctl enable logp
# Configure Nginx
cat > logp << %EOF%
server {
    listen 80;
    server_name _;

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/logp/logpapp/logp.sock;
    }
}
%EOF%
sudo mv logp /etc/nginx/sites-available
sudo ln -s /etc/nginx/sites-available/logp /etc/nginx/sites-enabled
sudo rm /etc/nginx/sites-enabled/default
sudo systemctl restart nginx
