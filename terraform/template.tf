 variable "project_name"  {
  type = string
  description = "GCP project name"
}

variable "region_name" {
  type = string
  description = "GC region to use"
}

variable "zone_name" {
  type = string
  description = "GC zone to use"
}

variable "user_name" {
  type = string
  description = "user name on local machine"
}

variable "public_key_path" {
  type = string
  description = "path to public key file"
}

variable "logpserver_name" {
  type = string
  description = "name for logpserver instance"
  default = "logpserver"
}

variable "logpserver_type" {
  type = string
  description = "logpserver instance type"
  default = "f1-micro"
}

variable "logpserver_image_id" {
  type = string
  description = "id of image used to create logpserver instance"
}

provider "google" {
  project = var.project_name
  region  = var.region_name
  zone    = var.zone_name
}

resource "google_compute_firewall" "default" {
  name    = "logp-firewall"
  network = google_compute_network.default.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80"]
  }

}

resource "google_compute_network" "default" {
  name = "logp-network"
}
resource "google_compute_instance" "logpserver" {
  name = var.logpserver_name
  machine_type = var.logpserver_type
  allow_stopping_for_update = "true"

  metadata = {
    ssh-keys = "${var.user_name}:${file(var.public_key_path)}"
    }

  metadata_startup_script = <<-EOF
        #! /bin/sh
        #sudo python3 /usr/local/lib/python3.8/dist-packages/logp/api.py &
  EOF

  boot_disk {
    initialize_params {
      image = var.logpserver_image_id
    }
  }

  network_interface {
    network = google_compute_network.default.name
    access_config {
    }
  }

  service_account {
    scopes = ["cloud-platform"]
  }
} 

// A variable for extracting the external ip of the logpserver
output "logpserver-url" {
  value = "${google_compute_instance.logpserver.network_interface.0.access_config.0.nat_ip}"
}
