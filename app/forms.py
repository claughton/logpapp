from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField
from wtforms.validators import DataRequired, ValidationError
from app.models import User
from flask_wtf.file import FileField, FileRequired, FileAllowed


class LoginForm(FlaskForm):
    student_id = StringField('Student ID', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

    def validate_student_id(self, student_id):
        user = User.query.filter_by(student_id=student_id.data).first()
        if user is None:
            raise ValidationError('Unknown Student ID')

class UploadForm(FlaskForm):
    cdxfile = FileField('ChemDraw file', validators=[FileRequired(), FileAllowed(['cdx'], '.cdx file only!')])
    submit = SubmitField('Submit')
