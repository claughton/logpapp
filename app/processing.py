import subprocess
import os.path as op
import tempfile
import csv

def import_users(csvfile):
    users = []
    with open(csvfile, newline='', encoding='utf-8-sig') as c:
        creader = csv.DictReader(c)
        for row in creader:
                users.append(row)
    return users
    
def extract_logps(cdxfile):
    result = subprocess.run(['obabel', cdxfile, '-otext', '-ad'],
                       stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE)
    logps = []
    for line in result.stdout.decode('utf-8').split('\n'):
        if 'Log P' in line:
            logps.append(float(line[-6:-1]))
    return logps

def extract_smiles(cdxfile):
    result = subprocess.run(['obabel', cdxfile, '-osmi'],
                       stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE)
    smiles = [s[:-1] for s in result.stdout.decode('utf-8').split('\n') if len(s) > 0]
    return smiles

def smiles_to_png(smiles):
    pngfile = tempfile.NamedTemporaryFile(suffix='.png')
    smifile = tempfile.NamedTemporaryFile(suffix='.smi')
    with open(smifile.name, 'w') as f:
        f.write(smiles)
    
    result = subprocess.run(['obabel', smifile.name, '-O', pngfile.name],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
    with open(pngfile.name, 'rb') as f:
        data = f.read()
    return data

def logps_to_score(logps, targets):
    error = 0.0
    for l, t in zip(logps, targets):
        e = l - t
        if e < 0.0:
            e = -e
        error += e
    return 10.0 - error
    
    lp = np.array(logps)
    error = lp - np.array([1.5, 2.5, 3.5, 4.5])
    score = 10.0 - np.abs(error).sum()
    return score
