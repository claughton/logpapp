from datetime import datetime
from app import db, login
from flask_login import UserMixin

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    student_id = db.Column(db.String(64), index=True, unique=True)
    first_name = db.Column(db.String(64), index=True)
    surname = db.Column(db.String(64), index=True)
    has_submitted = db.Column(db.Boolean(), index=True, default=False)
    entry = db.relationship('Entry', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<User {} {}>'.format(self.first_name, self.surname)   

class Entry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    cdxfile = db.Column(db.String(64))
    smiles1 = db.Column(db.String(64))
    smiles2 = db.Column(db.String(64))
    smiles3 = db.Column(db.String(64))
    smiles4 = db.Column(db.String(64))
    png1 = db.Column(db.String(64))
    png2 = db.Column(db.String(64))
    png3 = db.Column(db.String(64))
    png4 = db.Column(db.String(64))
    logp1 = db.Column(db.Float())
    logp2 = db.Column(db.Float())
    logp3 = db.Column(db.Float())
    logp4 = db.Column(db.Float())
    score = db.Column(db.Float(), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

def __repr__(self):
    return '<Entry {}>'.format(self.user_id)

@login.user_loader
def load_user(id):
    return User.query.get(int(id))
