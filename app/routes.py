from flask import render_template, request, redirect, url_for, flash
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Entry
from app.processing import extract_logps, extract_smiles, smiles_to_png, logps_to_score, import_users
from app import app, db
from app.forms import LoginForm, UploadForm
from werkzeug.utils import secure_filename
from werkzeug.urls import url_parse
import os.path as op
import uuid
import logging

logging.basicConfig(filename='logpapp.log')

users = import_users('students2021.csv')
current_users = User.query.all()
current_ids = [cu.student_id for cu in current_users]
for user in users:
    if not user['ID number'] in current_ids:
        u = User()
        u.student_id = user['ID number']
        u.first_name = user['First name']
        u.surname = user['Surname']
        db.session.add(u)
    db.session.commit()

@app.route('/')
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    if current_user.has_submitted:
        return redirect(url_for('leaderboard'))
    return redirect(url_for('submit'))

@app.route('/submit', methods=['GET', 'POST'])
@login_required
def submit():
    if current_user.has_submitted:
        return redirect(url_for('leaderboard'))
    form = UploadForm()
    if form.validate_on_submit():
        f = form.cdxfile.data
        filename = str(uuid.uuid4()) + '.cdx'
        filepath = op.join(app.config['UPLOAD_FOLDER'], filename)
        f.save(filepath)
        current_user.has_submitted = True
        e = Entry(cdxfile=filename, author=current_user)
        logps = extract_logps(filepath)
        if len(logps) < 4:
            flash("Can't find 4 logps in file!")
            return redirect(url_for('index'))
        smiles = extract_smiles(filepath)
        if len(smiles) < 4:
            flash("Can't find 4 molecules in file!")
            return redirect(url_for('index'))
        e.logp1 = logps[0]
        e.logp2 = logps[1]
        e.logp3 = logps[2]
        e.logp4 = logps[3]
        e.smiles1 = smiles[0]
        e.smiles2 = smiles[1]
        e.smiles3 = smiles[2]
        e.smiles4 = smiles[3]
        pngfiles = []
        for smile in smiles:
            pngfile = str(uuid.uuid4()) + '.png'
            pngfiles.append(pngfile)
            pngpath = op.join(app.config['UPLOAD_FOLDER'], pngfile)
            with open(pngpath, 'wb') as f:
                f.write(smiles_to_png(smile))

        e.png1 = pngfiles[0]
        e.png2 = pngfiles[1]
        e.png3 = pngfiles[2]
        e.png4 = pngfiles[3]
        score = logps_to_score(logps, [1.5, 2.5, 3.5, 4.5])
        e.score = score
        db.session.add(e)
        db.session.commit()
        return redirect(url_for('score'))
    return render_template('index.html', title='Submit', form=form)

@app.route('/display/<filename>')
def display_image(filename):
    return redirect(url_for('static', filename='uploads/' + filename), code=301)

@app.route('/score')
@login_required
def score():
    if not current_user.has_submitted:
        return redirect(url_for('submit'))

    entries = [e for e in current_user.entry.all() if e.score != None]
    if len(entries) == 0:
        current_user.has_submitted = False
        db.session.commit()
        return redirect(url_for('submit'))
    return render_template('score.html', title='Your Score', entry=entries[0])
    
@app.route('/leaderboard')
@login_required
def leaderboard():
    if not current_user.has_submitted:
        return redirect(url_for('submit'))

    #entries = [e for e in current_user.entry.all() if e.score != None]
    entries = [e for e in Entry.query.all() if e.score != None]
    entries.sort(key=lambda e: -e.score)
    return render_template('leaderboard.html', title='Leaderboard', ranked_entries=entries)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(student_id=form.student_id.data).first()
        if user is None:
            flash('Invalid student id')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/delete')
@login_required
def delete():
    entries = [e for e in current_user.entry.all()]
    for e in entries:
        db.session.delete(e)
    current_user.has_submitted = False
    db.session.commit()
    return redirect(url_for('index'))
